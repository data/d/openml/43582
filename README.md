# OpenML dataset: Pima-Indians-Diabetes

https://www.openml.org/d/43582

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

DESCRIPTION
Problem Statement
    NIDDK (National Institute of Diabetes and Digestive and Kidney Diseases) research creates knowledge about and treatments for the most chronic, costly, and consequential diseases.
    The dataset used in this project is originally from NIDDK. The objective is to predict whether or not a patient has diabetes, based on certain diagnostic measurements included in the dataset.
    Build a model to accurately predict whether the patients in the dataset have diabetes or not.
Dataset Description
The datasets consists of several medical predictor variables and one target variable (Outcome). Predictor variables includes the number of pregnancies the patient has had, their BMI, insulin level, age, and more.
Variables    Description
Pregnancies    Number of times pregnant
Glucose    Plasma glucose concentration in an oral glucose tolerance test
BloodPressure    Diastolic blood pressure (mm Hg)
SkinThickness    Triceps skinfold thickness (mm)
Insulin    Two hour serum insulin
BMI    Body Mass Index
DiabetesPedigreeFunction    Diabetes pedigree function
Age    Age in years
Outcome    Class variable (either 0 or 1). 268 of 768 values are 1, and the others are 0
Inspiration

Predict whether or not a patient has diabetes, based on certain diagnostic measurements included in the dataset.
Build a model to accurately predict whether the patients in the dataset have diabetes or not.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43582) of an [OpenML dataset](https://www.openml.org/d/43582). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43582/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43582/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43582/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

